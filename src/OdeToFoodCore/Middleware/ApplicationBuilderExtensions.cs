﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace OdeToFoodCore.Middleware
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseNodeModules(this IApplicationBuilder app, IHostingEnvironment env)
        {
            var path = Path.Combine(env.ContentRootPath, "node_modules");

            var options = new StaticFileOptions();
            options.RequestPath = "/node_modules";
            options.FileProvider = new PhysicalFileProvider(path);

            app.UseStaticFiles(options);
            return app;
        }
    }
}
