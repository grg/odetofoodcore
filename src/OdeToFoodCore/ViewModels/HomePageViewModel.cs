﻿using System.Collections.Generic;
using OdeToFoodCore.Entities;

namespace OdeToFoodCore.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
    }
}
