﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OdeToFoodCore.Controllers
{
    [Route("company/[controller]/[action]")]
    public class InfoController
    {
        public string Phone()
        {
            return "12345678";
        }

        public string Country()
        {
            return "Denmark";
        }
    }
}
